const grades = [
    {
        "id": "3689e68ad161e733c560a40d7af78114",
        "lastName": "Doe",
        "group": "A",
        "subject": "Math",
        "ticketNumber": 12345,
        "grade": 95,
        "teacher": "Smith"
    },
    {
        "id": "1e80852dca982178b3d7eabffa54382e",
        "lastName": "Smith",
        "group": "B",
        "subject": "Science",
        "ticketNumber": 54321,
        "grade": 85,
        "teacher": "Johnson"
    },
    {
        "id": "c0a7c2a8d715a009397677b3e1a40b18",
        "lastName": "Johnson",
        "group": "A",
        "subject": "History",
        "ticketNumber": 67890,
        "grade": 75,
        "teacher": "Doe"
    },
];

module.exports = {
    grades,
};
    